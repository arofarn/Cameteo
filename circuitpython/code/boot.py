# Selectively setting readonly to False on boot

import board
import digitalio
import storage

jumper = digitalio.DigitalInOut(board.D5)
jumper.direction = digitalio.Direction.INPUT
jumper.pull = digitalio.Pull.UP
# led = digitalio.DigitalInOut(board.D13)
# led.direction = digitalio.Direction.OUTPUT

# If the jumper on D5 is connected
# CircuitPython can write to the drive
storage.remount("/", jumper.value)
# led.value = jumper.value
print("Read-only: {}".format(jumper.value))
