# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
##########################
# Weather and GPS logger #
##########################

Use with:
 * Adafruit Feather M4 Express (CircuitPython firmware)
 * Adafruit Ultimate GPS FeatherWing
 * Bosch BME280 sensor (air temperature, humidity, atmospheric pressure) on I2C

Pins used:
 * TX/RX : Ultimate GPS FeatherWing (data)
 * A5 : Ultimate GPS FeatherWing EN (enabe/disable GPS power)
 * I2C SDA/SCL : BME280 sensor
 * D5 : RW / RO jumper
 * A2, A3 : UART TX & RX to Raspberry pi (115200 baudrate)
 * D9 : to EN pin on Raspberry Lipo SHIM
 * D13 : onboard LED (as RW/RO indicator)

Back pins diagramm:
    D05 A2  A3  D09 NC
    GND GND SCL SDA 3V3

    RW  TX  RX  EN  NC
    GND GND SCL SDA 3V3

Git repository :
https://framagit.org/arofarn/Cameteo

TODO for v1 :
 * write data on flash drive (work-in-progress)
 * send data through UART (work-in-progress)
"""
__author__ = "arofarn"
__version__ = 0.3

#######################
import time
import gc
import rtc
import board
from digitalio import DigitalInOut, Direction
from busio import UART
# import micropython
import neopixel
import cameteo

##########
# config #
##########

PRINT_DATA = False           # Print data on USB UART / REPL output ?
DATA_TO_NEOPIXEL = True     # Display atmospheric data as color on onboard neopixel ?
GPS_ENABLE = True           # Use GPS module ?
UPDATE_INTERVAL = 10        # Interval between data acquisition (in seconds)
WRITE_INTERVAL = 60         # Interval between data written on flash Memory (-1 to disable)
SEND_INTERVAL = 60          # Interval between packet of data sent to Rpi (-1 to disable)
NEOPIXEL_MAX_VALUE = 70     # max value instead of brightness to spare some mem

#########
# Setup #
#########

gc.collect()
# micropython.mem_info()

#Set ENable pin for Raspberry power microcontrol
RPI_EN = DigitalInOut(board.D9)
RPI_EN.direction = Direction.OUTPUT
RPI_EN.value = True

# Enable RTC of the feather M0 board
CLOCK = rtc.RTC()

# Second UART to communicate with raspberry pi (throught GPIO)
if SEND_INTERVAL >= 0:
    rpi_uart = UART(board.A2, board.A3, baudrate=115200, timeout=2000)

# Set onboard Neopixel : used as atmo data output
# brightness is fixed to 1 to spare some memory. Use NEOPIXEL_MAX_VALUE instead
if DATA_TO_NEOPIXEL:
    pixel = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=1)
else:
    # if neopixel is disable : turn off the LED
    pixel = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=1)
    pixel[0] = (0, 0, 0)
    del pixel

sys_data = cameteo.SysData(debug=False)
bme_data = cameteo.BME280Data(debug=False)
gps_data = cameteo.GPSData(debug=False)

data = [sys_data, gps_data, bme_data]

rtc.set_time_source(gps_data)
CLOCK_SYNCED = False

#############
# Main loop #
#############

while True:

    current_time = time.monotonic()

    for src in data:
        src.update(current_time, verbose=PRINT_DATA)

    # Sync clocks
    CLOCK_SYNCED = cameteo.set_clock_from_localtime(CLOCK, threshold=2.0, debug=False)

    for src in data:
        #Only write data when clocks are OK
        if CLOCK_SYNCED:
            src.write_on_flash(current_time)
        src.send_json(current_time, uart=rpi_uart, verbose=True)

        gc.collect()
        # micropython.mem_info(1)
        # print('Memory free: {} allocated: {}'.format(gc.mem_free(), gc.mem_alloc()))
