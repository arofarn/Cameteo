# -*- coding: utf-8 -*-
"""
Eléments communs au projet Camétéo

Created on Fri Aug 18 21:35:59 2017

@author: arofarn
"""

###########
# IMPORTS #
###########

from cameteo_conf import *
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

#####################
# Flask & SQLAchemy #
#####################

app = Flask(__name__)
app.config['SERVER_NAME'] = hostname + ":" + http_listen_port
app.config['SECRET_KEY'] = flask_secret
app.config['DEBUG'] = flask_debug
app.config['SQLALCHEMY_DATABASE_URI'] = sql_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['BOOTSTRAP_USE_MINIFIED'] = False
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['IMAGES_URL'] = flask_images_url
app.config['IMAGES_PATH'] = flask_images_path
app.config['IMAGES_CACHE'] = flask_images_cache

#app.config['SQLALCHEMY_POOL_RECYCLE'] = 600

######################
# SQLAlchemy Classes #
######################

db = SQLAlchemy(app)

class SensorModel(db.Model):
    """SensorModel : classe des modèles de capteur"""

    __tablename__= 'sensor_model'

    name=db.Column(db.String(20), primary_key=True)
    brand=db.Column(db.String(20), nullable=False)
    description=db.Column(db.String(1000))

    def __repr__(self):
        return "<SensorModel(name='%s', brand='%s', description='%s')>" % (self.name, self.brand, self.description)

    def __str__(self):
        return "%s %s" % (self.brand, self.name)

class Sensor(db.Model):
    """Sensor : classe des capteurs"""

    __tablename__= 'sensors'

    name = db.Column(db.String(20), primary_key=True)
    model_id = db.Column(db.String(20), nullable=False)
    description = db.Column(db.String(1000))

    def __repr__(self):
        return "<Sensor(name='%s', model='%s', description='%s')>" % (self.name, self.model, self.description)

    def __str__(self):
        return """%s :
 - Modèle : \t%s
 - Description: \t%s""" % (self.name, self.model, self.description)

class DataType(db.Model):
    """DataType : classe des types de données"""

    __tablename__ = 'data_type'

    type_id = db.Column(db.String(4), primary_key=True)
    name = db.Column(db.String(20), nullable=False)       #Short name
    plot_mini = db.Column(db.Float, nullable=True)        #y minimum for plotting
    plot_maxi = db.Column(db.Float, nullable=True)        #y maximum for plotting
    unit_si = db.Column(db.String(10), nullable=True)     #Unit in SI
    description = db.Column(db.String(1000))              #Long description

    def __repr__(self):
        return "<DataType(name='%s', ID='%s', description='%s')>" % (self.name, self.type_id, self.description)

    def __str__(self):
        return "%s (%s)" % (self.name, self.unit)

class Data(db.Model):
    """Data : classe des données"""

    __tablename__ = 'data'

    data_id = db.Column(db.BigInteger, primary_key=True, autoincrement="auto")
    dbdate = db.Column(db.DateTime, nullable=False)
    valdate = db.Column(db.DateTime, nullable=True)
    value = db.Column(db.Float, nullable=True)
    unit = db.Column(db.String(20), nullable=True)
    type_id = db.Column(db.String(4), nullable=True)
    sensor_id = db.Column(db.String(20), nullable=True)

    def __repr__(self):
        return "<Data(value='%0.2f', type='%s', date='%s', sensor ID='%s', id='%s')>" % (self.value, self.type_id, self.valdate.utc, self.sensor_id, self.data_id)

    def __str__(self):
        try:
            val = "{:8.2f} {:8s}".format(self.value, self.unit)
        except:
            val = "  'NULL'         "
        return "{} : {:4s}= {} (capteur: {:s})".format(self.valdate, self.type_id, val, self.sensor_id)

class Photo(db.Model):
    """DataType : classe des types de données"""

    __tablename__ = 'photos'

    photo_id = db.Column(db.BigInteger, primary_key=True, autoincrement="auto")
    file_name = db.Column(db.String(32), unique = True, index = True, nullable=False)
    file_date = db.Column(db.DateTime, nullable=False)
    image_type = db.Column(db.String(4), nullable=False)

    def __repr__(self):
        return "<Photo(name='%s', date='%s', ID='%s')>" % (self.file_name, self.photo_id, self.description)

    def __str__(self):
        return "%s" % (self.file_name)
