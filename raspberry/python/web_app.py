#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 17 22:25:52 2017

@author: arofarn
"""

###########
# IMPORTS #
###########

from cameteo import *
from flask import render_template
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import *
import pandas as pd
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.models import ColumnDataSource
from bokeh.resources import INLINE

########
# MAIN #
########

bootstrap = Bootstrap(app)

nav = Nav()
nav.init_app(app)

navbar = Navbar('Camétéo',
                View('Accueil', 'index'),
                View('Toutes les données', 'all_data'),
                Subgroup('Par données',
                        View("Température de l'air",            'by_data_type', dt='AT'),
                        View("Humidité relative",               'by_data_type', dt='RH'),
                        View("Pression atmosphérique (locale)", 'by_data_type', dt='AP'),
                        View("Pression atmosphérique (mer)",    'by_data_type', dt='MSLP'),
                        View("Altitude",                        'by_data_type', dt='ALTI'),
                        ),
                Subgroup('Par capteur',
                        View("Capteur BME280", 'by_sensor', sens = 'AdaBME280_1')
                        ),
                Subgroup('Photos',
                         View('Dernière Photo', 'picture', num=0),
                         View('Première Photo', 'picture', num=999999999),
                        ),
                View('Configuration', 'config_page')
                )

nav.register_element('top', navbar)

js_resources = INLINE.render_js()
css_resources = INLINE.render_css()

@app.route('/')
def index():
    return render_template('index.html.j2')

@app.route('/picture=<num>')
def picture(num):
    num=int(num)
    if num < 0 :
        num = 0
    pictures = Photo.query.order_by(Photo.file_date.desc())
    if num > pictures.count()-1:
        num = pictures.count()-1
    pict = pictures[num]
    return render_template('photos.html.j2', picture_path = os.path.join("static/photos", pict.file_name), numero = num )

@app.route('/all_data')
def all_data():
    date_deb = datetime.utcnow()-timedelta(seconds=3600)
    res = Data.query.filter(Data.dbdate >= date_deb)
    return render_template('data_viz.html.j2', dat=res.order_by(Data.dbdate.desc()).paginate(per_page=15))

@app.route('/type_id=<dt>')
def by_data_type(dt):
    date_deb = datetime.now()-timedelta(hours=1)
    #Récupération des données à afficher
    q = Data.query.filter(Data.type_id == dt).filter(Data.dbdate >= date_deb).order_by(Data.valdate)
    #q = Data.query.filter(Data.type_id == dt).order_by(Data.valdate)
    res = pd.read_sql(q.selectable, db.engine)

    plot_data = ColumnDataSource(data=dict(dates = res['data_valdate'], values = res['data_value']))

    #Préparation du graphique
    data_plot = figure(tools        = "hover,pan,reset,save,wheel_zoom",
                       title        = dt,
                       x_axis_label = 'Date',
                       x_axis_type  = 'datetime',
                       y_axis_label = dt,
                       plot_height  = 400,
                       plot_width   = 1140,
                       )
    data_plot.line('dates', 'values', source=plot_data, line_width=2)

    script, div = components(data_plot)

    return render_template('data_graph.html.j2',
                            data_type = dt,
                            dat=q.order_by(Data.dbdate.desc()).paginate(per_page=15),
                            plot_script=script,
                            plot_div=div,
                            js_resources=js_resources,
                            css_resources=css_resources,
                           )

@app.route('/sensor_id=<sens>')
def by_sensor(sens):
    date_deb = datetime.utcnow()-timedelta(seconds=3600)
    res = Data.query.filter(Data.sensor_id == sens).filter(Data.dbdate >= date_deb)
    return render_template('data_viz.html.j2', dat=res.order_by(Data.dbdate.desc()).paginate(per_page=15))

@app.route('/configuration', methods=['GET', 'POST'])
def config_page():

    if request.method  == 'POST':
        pass
    else:
        pass

    return render_template('config_page.html.j2')

@app.route('/json_type_id=<dt>')
def json_by_data_type(dt):
    date_deb = datetime.now()-timedelta(hours=1)
    #Récupération des données à afficher
    q = Data.query.filter(Data.type_id == dt).filter(Data.dbdate >= date_deb).order_by(Data.valdate)
    #q = Data.query.filter(Data.type_id == dt).order_by(Data.valdate)
    res = pd.read_sql(q.selectable, db.engine)

    return res.to_json()

if __name__=="__main__":
    app.run(host="0.0.0.0")
