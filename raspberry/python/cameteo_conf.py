# -*- coding: utf-8 -*-
"""
Configuration du projet Camétéo

Created on Fri Aug 18 21:35:59 2017

@author: arofarn
"""

###########
# IMPORTS #
###########

from datetime import datetime, timedelta, timezone
from configparser import SafeConfigParser
import os, sys, socket

#################
# CONFIGURATION #
#################

hostname = socket.gethostname() + ".local"

script_path = os.path.dirname(sys.argv[0])
script_dir = os.path.abspath(script_path)

parser = SafeConfigParser(allow_no_value=True)
parser.read(script_dir + '/cameteo.conf')

#Misc
TimeZone = timezone(timedelta(hours=int(parser['MISC'].get('TimeZone', fallback=0))))

#SQL
sql_host    = parser['SQL'].get('sql_host', fallback='localhost')
sql_port    = parser['SQL'].get('sql_port', fallback=3306)
sql_base    = parser['SQL'].get('sql_base', fallback='cameteo')
sql_user    = parser['SQL'].get('sql_user', fallback='cameteo')
sql_pass    = parser['SQL'].get('sql_pass', fallback='oetemac')
sql_sys     = parser['SQL'].get('sql_sys', fallback='mysql')
sql_api     = parser['SQL'].get('sql_api', fallback='pymysql')
sql_verbose = bool(int(parser['SQL'].get('sql_verbose', fallback=0)))

sql_uri = sql_sys + '+' + sql_api + '://' + sql_user + ':' + sql_pass + '@' + sql_host + ':' + sql_port + '/' + sql_base

#MQTT
mqtt_host       = parser['MQTT'].get('mqtt_host', fallback='localhost')
mqtt_port       = int(parser['MQTT'].get('mqtt_port', fallback=1883))
mqtt_client_id  = parser['MQTT'].get('mqtt_client_id', fallback='mqtt2sql')
mqtt_user       = parser['MQTT'].get('mqtt_user', fallback='cameteo')
mqtt_pass       = parser['MQTT'].get('mqtt_pass', fallback='oetemac')
mqtt_auth       = {'username' : mqtt_user, 'password' : mqtt_pass}
mqtt_qos        = parser['MQTT'].get('mqtt_qos', fallback=0)
mqtt_topic      = parser['MQTT'].get('mqtt_topic', fallback='sensors/#')

#HTTP FLASK
http_host           = parser['FLASK'].get('http_host', fallback='127.0.0.1')
http_listen_port    = parser['FLASK'].get('http_listen_port', fallback='5000')

flask_debug         = bool(int(parser['FLASK'].get('flask_debug', fallback=1)))
flask_secret        = parser['FLASK'].get('flask_secret', fallback='random')

flask_images_url    = parser['FLASK'].get('flask_images_url', fallback='/urlsizer')
flask_images_path   = parser['FLASK'].get('flask_images_path', fallback='static/photos')
flask_images_cache  = parser['FLASK'].get('flask_images_cache', fallback='cache/flask-images')

#Camera
camera_mqtt_topic   = parser['CAMERA'].get('camera_mqtt_topic', fallback='raspi0/camera')
camera_resolution_x = int(parser['CAMERA'].get('camera_resolution_x', fallback='800'))
camera_resolution_y = int(parser['CAMERA'].get('camera_resolution_y', fallback='600'))
camera_resolution   = (camera_resolution_x, camera_resolution_y)
camera_warmup_time  = int(parser['CAMERA'].get('camera_warmup_time', fallback=2))
camera_iso          = int(parser['CAMERA'].get('camera_iso', fallback=0))
camera_awb          = parser['CAMERA'].get('camera_auto_white_balance', fallback='auto')
camera_expo_mode    = parser['CAMERA'].get('camera_exposure_mode', fallback='auto')
camera_rotation     = int(parser['CAMERA'].get('camera_rotation', fallback=0))
camera_contrast     = int(parser['CAMERA'].get('camera_contrast', fallback=0))

photo_dir        = parser['CAMERA'].get('photo_dir', fallback='/home/pi/Cameteo/photos/')
photo_name       = parser['CAMERA'].get('photo_name', fallback='%%Y%%m%%d_%%H%%M%%S')
photo_format     = parser['CAMERA'].get('photo_format', fallback='jpg')
photo_extensions = {'jpeg': 'jpg',
                    'png' : 'png',
                    'gif': 'gif',
                    'bmp': 'bmp',
                    'yuv': 'yuv',
                    'rgb': 'rgb',
                    'rgba': 'rgba'}

#epaper display
epd_font_file           =  parser['EPD'].get('epd_font_file'        , fallback='/usr/share/fonts/truetype/freefont/FreeMono.ttf')
epd_bold_font_file      =  parser['EPD'].get('epd_bold_font_file'   , fallback='/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf')
epd_italic_font_file    =  parser['EPD'].get('epd_italic_font_file' , fallback='/usr/share/fonts/truetype/freefont/FreeMonoOblique.ttf')
epd_rotate              =  min(int(parser['EPD'].get('epd_rotate', fallback=0)), 1)

########
# MISC #
########

# Dictionnary of data types and units
data_type = {'cput' :   "CPUT",
             'temp':    "AT",
             'time':    "TIME",
             'hum':     "RH",
             'press':   "AP",
             'vbat':    "VBAT",
             'lat':     "COOR",
             'lon':     "COOR",
             'alt':     "ALTI",
             'pasl':    "MSPL",
             'qual':    "MISC",
             'age':     "MISC"}
data_unit = {'cput' :   "degC",
             'temp':    "degC",
             'time':    "",
             'hum':     "%",
             'press':   "hPa",
             'vbat':    "V",
             'lat':     "deg",
             'lon':     "deg",
             'alt':     "m",
             'pasl':    "hPa",
             'qual':    "",
             'age':     "s"}
