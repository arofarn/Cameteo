#!/usr/bin/python3
# -*- coding: UTF8 -*-

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Take a picture with the Pi Camera module and publish the path
and name of the file on a MQTT broker

@author: arofarn
"""

import os
from time import sleep
from datetime import datetime
import picamera
import paho.mqtt.publish as mqtt
from cameteo_conf import CONFIG_MQTT, CONFIG_PI_CAMERA, PICTURES_EXTENSIONS

MQTT_CLIENT_ID = "picamera"
NOW = datetime.utcNOW()

########
# Main #
########

if not os.path.exists(CONFIG_PI_CAMERA['PICT_DIR']):
    print("Create photos directory : " + CONFIG_PI_CAMERA['PICT_DIR'])
    os.mkdir(CONFIG_PI_CAMERA['PICT_DIR'])

FILE_NAME = os.path.extsep.join((datetime.strftime(NOW, CONFIG_PI_CAMERA['PICT_NAME']),
                                 PICTURES_EXTENSIONS[CONFIG_PI_CAMERA['PICT_FORMAT']]))
FILE_PATH = os.path.join(CONFIG_PI_CAMERA['PICT_DIR'], FILE_NAME)

print("Shooting " + FILE_PATH)

# Taking a photo
#################

# Tell the MQTT that we start shooting
mqtt.single(CONFIG_PI_CAMERA['MQTT_TOPIC'] + "/shooting",
            payload=1,
            qos=1,
            retain=True,
            hostname=CONFIG_MQTT['HOST'],
            port=CONFIG_MQTT['PORT'],
            client_id=MQTT_CLIENT_ID,
            auth=CONFIG_MQTT['AUTH'])

# Camera settings
cam = picamera.PiCamera(resolution=CONFIG_PI_CAMERA['RESOLUTION'])
cam.iso = CONFIG_PI_CAMERA['ISO']
cam.awb_mode = CONFIG_PI_CAMERA['AWB']
cam.rotation = CONFIG_PI_CAMERA['ROTATION']
cam.contrast = CONFIG_PI_CAMERA['CONSTRAST']
cam.exposure_mode = CONFIG_PI_CAMERA['EXPO_MODE']
cam.image_effect = 'none'

#Add some EXIF tags
cam.exif_tags['EXIF.UserComment'] = 'Cameteo !'

# Warm-up
cam.start_preview()
sleep(CONFIG_PI_CAMERA['WARMUP_TIME'])

cam.capture(FILE_PATH, format=CONFIG_PI_CAMERA['PICT_FORMAT'],
            quality=95, thumbnail=None)

cam.close()

# MQTT publication
###################

MQTT_DATA = [{'topic': CONFIG_PI_CAMERA['MQTT_TOPIC'] + "/shooting",
              'payload': 0,
              'qos': 2,
              'retain': True},
             {'topic': CONFIG_PI_CAMERA['MQTT_TOPIC'] + "/photos_path",
              'payload': CONFIG_PI_CAMERA['PICT_DIR'],
              'qos': 2,
              'retain': True},
             {'topic': CONFIG_PI_CAMERA['MQTT_TOPIC'] + "/last_photo",
              'payload': CONFIG_PI_CAMERA['PICT_NAME'],
              'qos': 2,
              'retain': True},
            ]

mqtt.multiple(MQTT_DATA,
              hostname=CONFIG_MQTT['HOST'],
              port=CONFIG_MQTT['PORT'],
              client_id=MQTT_CLIENT_ID,
              auth=CONFIG_MQTT['AUTH'])
