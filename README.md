Projet personnel de petite station météo capable de prendre aussi des photos à pas de temps réguliers

En cours... // Work in progress...

Utilisations prévues :
 - randonnée pédestre et cycliste => autonomie et poids
 - documentation de culture : possible d'utiliser un appareil photo sur pieds.
 
Pour l'instant, le projet s'articule sur les composants matériels suivants :
 - Raspberry pi zero
 - module PiCamera v2 pour les photos (8 Mpx)
 - écran ePaper 2.13" Inky-PHAT de Pimoroni sur le GPIO (SPI)
 - carte Adafruit Feather M4 Express (SAMD51) sous CircuitPython (fork de MicroPython)
 - capteur Bosch BME280 : capteur de température, pression atmosphérique et humidité
 - Adafruit Ultimate GPS Feather : module GPS pour positionnement et horodatage
 - boitier réalisé en impression 3D