<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="datacam" ID="ID_940180243" CREATED="1445275007181" MODIFIED="1445275113016"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Traitement donn&#xe9;es" POSITION="right" ID="ID_1847430113" CREATED="1445275240369" MODIFIED="1445275249403">
<node TEXT="Python (Rasp. Pi)" ID="ID_270658498" CREATED="1445275265758" MODIFIED="1447080486986">
<node TEXT="class Raw_Data" ID="ID_1595052898" CREATED="1445275279482" MODIFIED="1447080748503">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="class Calculated_Data" ID="ID_718485900" CREATED="1445275812612" MODIFIED="1447080463621"/>
<node TEXT="Fonction : Ajout des donn&#xe9;es &#xe0; une photo" ID="ID_627725476" CREATED="1445275595946" MODIFIED="1445275873150">
<node TEXT="Incrustation" ID="ID_733470856" CREATED="1445275875550" MODIFIED="1445275883425">
<node TEXT="choix ordre" ID="ID_1297866337" CREATED="1445275719545" MODIFIED="1445275726053"/>
<node TEXT="choix donn&#xe9;es affich&#xe9;e" ID="ID_971249184" CREATED="1445275726924" MODIFIED="1445275736408"/>
</node>
<node TEXT="Metadonn&#xe9;e EXIF et/ou XMP" ID="ID_908814226" CREATED="1445275885056" MODIFIED="1445275898061"/>
</node>
<node TEXT="Croisement avec donn&#xe9;es publiques" ID="ID_816020813" CREATED="1447081168096" MODIFIED="1447081190356">
<node TEXT="Sources" ID="ID_1534956111" CREATED="1447081192190" MODIFIED="1447081202255">
<node TEXT="M&#xe9;t&#xe9;oFrance" ID="ID_960146793" CREATED="1447081205560" MODIFIED="1447081210611"/>
<node TEXT="NOAA (GFS / WRF)" ID="ID_484693743" CREATED="1447081217776" MODIFIED="1447081234298"/>
</node>
<node TEXT="Traitements" ID="ID_1239862080" CREATED="1447081274440" MODIFIED="1447081284372">
<node TEXT="Recalcul des altitudes (croisement pressions mesur&#xe9;e par datacam/pression organisme m&#xe9;t&#xe9;o/ coordonn&#xe9;es GPS)" ID="ID_3490593" CREATED="1447081287034" MODIFIED="1447081341959"/>
<node TEXT="Avertissement en cas de divergence importantes avec pr&#xe9;visions" ID="ID_1887237474" CREATED="1447081368574" MODIFIED="1447081389978"/>
</node>
</node>
</node>
<node TEXT="Embarqu&#xe9; Arduino" ID="ID_785409314" CREATED="1447080488757" MODIFIED="1447080501360">
<node TEXT="Sur p&#xe9;riode" ID="ID_1086659337" CREATED="1447080504005" MODIFIED="1447080515713">
<node TEXT="Moyenne" ID="ID_1592586385" CREATED="1447080517032" MODIFIED="1447080519825"/>
<node TEXT="Ecart-type" ID="ID_592958817" CREATED="1447080521647" MODIFIED="1447080531113"/>
<node TEXT="Quantiles" ID="ID_1704132109" CREATED="1447080532182" MODIFIED="1447080704522"/>
</node>
</node>
</node>
<node TEXT="Acquisition photo" POSITION="left" ID="ID_205007013" CREATED="1445275114354" MODIFIED="1445275124232">
<node TEXT="PiCam" ID="ID_471792515" CREATED="1445275165918" MODIFIED="1445275170779">
<node TEXT="Probl&#xe8;me 1 : cable plat tr&#xe8;s peu pratique" ID="ID_279048394" CREATED="1447417532604" MODIFIED="1447417604312"/>
<node TEXT="Probl&#xe8;me 2 : connection tr&#xe8;s sensible (facilement des probl&#xe8;mes de d&#xe9;tection" ID="ID_179270345" CREATED="1447417551524" MODIFIED="1447417586481"/>
</node>
<node TEXT="gPhoto2" ID="ID_714902196" CREATED="1445275175392" MODIFIED="1445275366515">
<icon BUILTIN="help"/>
</node>
<node TEXT="webcam USB" ID="ID_598071482" CREATED="1445703078725" MODIFIED="1445703092031">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="Autonomie" POSITION="right" ID="ID_1591208209" CREATED="1445275213800" MODIFIED="1445275219743">
<node TEXT="UI" ID="ID_1359812832" CREATED="1445275221705" MODIFIED="1445275225795"/>
<node TEXT="Energie" ID="ID_1154122970" CREATED="1445275227314" MODIFIED="1445275235842">
<node TEXT="Allumage R-Pi par Arduino" ID="ID_317304726" CREATED="1445275454078" MODIFIED="1445275473445"/>
<node TEXT="Batterie tampon" ID="ID_266523548" CREATED="1445275481734" MODIFIED="1445275494432"/>
<node TEXT="Alimentation" ID="ID_1650279501" CREATED="1445275498672" MODIFIED="1445275508689">
<node TEXT="Panneau solaire" ID="ID_62589468" CREATED="1445275510537" MODIFIED="1445275517145"/>
<node TEXT="Dynamo v&#xe9;lo" ID="ID_187725160" CREATED="1445275518067" MODIFIED="1445275524512"/>
<node TEXT="Batterie grosse capacit&#xe9;" ID="ID_538264726" CREATED="1445275525409" MODIFIED="1445275544191"/>
</node>
</node>
<node TEXT="Boitier" ID="ID_378357561" CREATED="1445275554462" MODIFIED="1445275968197">
<node TEXT="r&#xe9;sistance &#xe0; l&apos;eau" ID="ID_1281039465" CREATED="1445275970340" MODIFIED="1445703138475">
<node TEXT="Id&#xe9;e 1 : impression 3D et/ou d&#xe9;coupe laser" ID="ID_77132726" CREATED="1447417185297" MODIFIED="1447417208635"/>
<node TEXT="Id&#xe9;e 2 : format bidon de cycliste (recyclage)" ID="ID_304162178" CREATED="1447417209434" MODIFIED="1447417236747"/>
</node>
<node TEXT="connectique" ID="ID_1602685639" CREATED="1445275980897" MODIFIED="1445275986634">
<node TEXT="alimentation" ID="ID_1585946812" CREATED="1445703179449" MODIFIED="1445703189353">
<node TEXT="panneau solaire" ID="ID_721407548" CREATED="1447417254583" MODIFIED="1447417273699"/>
<node TEXT="dynamo-v&#xe9;lo" ID="ID_629164198" CREATED="1447417278160" MODIFIED="1447417349222">
<node TEXT="prise USB classique (version &#xe9;tanche ???)" ID="ID_761396973" CREATED="1447417354551" MODIFIED="1447417370095"/>
<node TEXT="adaptateur direct vers la prise &#xe9;tanche existante" ID="ID_632440803" CREATED="1447417370816" MODIFIED="1447417410266"/>
</node>
</node>
<node TEXT="d&#xe9;port capteurs" ID="ID_574169682" CREATED="1445703190116" MODIFIED="1445703200861"/>
<node TEXT="d&#xe9;port acquisition photo" ID="ID_990319349" CREATED="1445703201468" MODIFIED="1445703213407"/>
</node>
<node TEXT="fixation" ID="ID_1152348868" CREATED="1445275993576" MODIFIED="1445275996969">
<node TEXT="v&#xe9;lo" ID="ID_311023012" CREATED="1445275998224" MODIFIED="1445276000394"/>
<node TEXT="rando" ID="ID_1679144491" CREATED="1445276001346" MODIFIED="1445276003720"/>
<node TEXT="tr&#xe9;pied photo" ID="ID_1795302368" CREATED="1445276004510" MODIFIED="1445276016077"/>
</node>
</node>
</node>
<node TEXT="Acquisition donn&#xe9;es" POSITION="left" ID="ID_1804667401" CREATED="1445275093295" MODIFIED="1445275570673">
<node TEXT="PiSense" ID="ID_684279078" CREATED="1445275152767" MODIFIED="1447080440543">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Arduino" ID="ID_1198550772" CREATED="1445275194375" MODIFIED="1445275205644">
<node TEXT="Formation" ID="ID_1233017322" CREATED="1445275253081" MODIFIED="1447080380438">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Mat&#xe9;riel" ID="ID_926052951" CREATED="1445275297254" MODIFIED="1445275304641">
<node TEXT="RTC" ID="ID_641589009" CREATED="1445275304644" MODIFIED="1447080383273">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="microSD" ID="ID_1775616220" CREATED="1445275316525" MODIFIED="1447080406240">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Temperature" ID="ID_1702799092" CREATED="1445275374067" MODIFIED="1447080408256">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Pression" ID="ID_1448376352" CREATED="1445275384406" MODIFIED="1447080411064">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Humidit&#xe9;" ID="ID_1888552872" CREATED="1445275387795" MODIFIED="1447080412960">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="GPS" ID="ID_1146102034" CREATED="1445275903847" MODIFIED="1451130798717">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="foudre" ID="ID_1917227809" CREATED="1445703116512" MODIFIED="1445703124790"/>
<node TEXT="Autre ?" ID="ID_1708926810" CREATED="1445275394108" MODIFIED="1445275397833"/>
</node>
<node TEXT="Logiciel" ID="ID_521818033" CREATED="1447080606285" MODIFIED="1447080611344">
<node TEXT="Donn&#xe9;es calcul&#xe9;es" ID="ID_238205261" CREATED="1447080619816" MODIFIED="1447080630924">
<node TEXT="Altitudes/Pression au niveau de la mer" ID="ID_145976984" CREATED="1447080633632" MODIFIED="1447080658177"/>
<node TEXT="Humidit&#xe9; / Point de ros&#xe9;e / Contenu en eau" ID="ID_253996014" CREATED="1447080661740" MODIFIED="1447080683496"/>
</node>
<node TEXT="Unification des capteur avec lib. Adafruit Sensors" ID="ID_1043083145" CREATED="1447080689744" MODIFIED="1447080736111"/>
</node>
</node>
</node>
</node>
</map>
